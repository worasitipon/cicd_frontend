import { KyInstance } from '~/helpers/ky';

export interface IReponseGetAllActiveComplementaryItems {
    complementary_id: number;
    erp_ax_id: string;
    item_code: string | null;
    complementary_name_2nd: string;
    complementary_name_en: string | null;
    specific_itemset_master_id: string | null;
    specific_itemset_detail_id: string | null;
    item_cost: number | null;
    item_list_price: number | null;
    item_sale_price: number | null;
}

export function getComplementaryItems(client: KyInstance) {
    return () => client.get('api/active/complementary').json<{ result: IReponseGetAllActiveComplementaryItems[] }>();
}
