import { Options } from '@nuxtjs/vuetify'

/**
 * nuxt vuetify configuration
 * @see https://github.com/nuxt-community/vuetify-module
 */
const vuetifyOptions: Options = {
    customVariables: [
        '~/assets/styles/vuetify_variables.scss',
    ],
    treeShake: true,
    defaultAssets: {
        font: {
            family: 'Bai Jamjuree',
        }
    }
}

export default vuetifyOptions;
