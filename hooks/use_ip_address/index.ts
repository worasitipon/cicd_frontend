import { computed } from '@vue/composition-api';
import { userStore } from '@/store/ip_address';

export const useIpAddress = () => {
    const store = userStore();
    const ipAddress = computed(() => store.getters.myIpAddress);
    const updateIpAddress = (newIp: string) => store.commit('UPDATE_IPADDRESS', newIp);

    return {
        ipAddress,
        updateIpAddress,
    };
};
