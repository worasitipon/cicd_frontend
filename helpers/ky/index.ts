import ky from 'ky-universal';

export type KyInstance = typeof ky;

export const client: KyInstance = ky.create({
    prefixUrl: process.env.APP_API_URL,
    retry: {
        limit: 3,
        statusCodes: [408, 413, 429, 502, 503, 504],
    },
});

export const clientNoPrefixUrl: KyInstance = client.extend({
    prefixUrl: undefined,
});

export const clientNoRetry: KyInstance = client.extend({
    retry: undefined,
});

export const clientWithRecaptcha = (token: string) => {
    const clientWithAuth: KyInstance = client.extend({
        hooks: {
            beforeRequest: [
                (_, options) => {
                    options.headers.set('Authorization', `Bearer ${token}`);
                },
            ],
        },
    });

    return clientWithAuth;
};

export function normalizeForSearchParams(obj: { [key: string]: any[] | string | number | undefined | null | Date }) {
    const keyList = Object.keys(obj);

    const result: { [key: string]: string | number } = {};

    keyList.forEach((key) => {
        const val = obj[key];

        if (val === undefined) {
            return;
        }

        if (val === null) {
            Reflect.set(result, key, null);
            return;
        }

        if (Array.isArray(val)) {
            Reflect.set(result, key, val.join(','));
            return;
        }

        if (typeof val === 'string' || typeof val === 'number' || val instanceof Date) {
            Reflect.set(result, key, `${val}`);
            return;
        }

        throw new Error(`${key} => unsupported type: ${typeof val}`);
    });

    return result;
}
