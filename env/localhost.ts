import { Configuration } from '@nuxt/types';

// APP
export const APP_VERSION: string = process.env.npm_package_version || 'undefined version';
export const APP_ENV = 'localhost';

export const APP_API_URL = 'https://dbpmixo885.execute-api.ap-southeast-1.amazonaws.com/Prod';
export const RECAPTCHA_SITE_KEY = '6LeyD5YUAAAAALV5TClDmW7itZwlUP1z7dXJalzv';

const env: Configuration['env'] = {
    APP_VERSION,
    APP_ENV,
    APP_API_URL,
    RECAPTCHA_SITE_KEY,
};

export default env;
