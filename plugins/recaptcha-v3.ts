import Vue from 'vue';
import { VueReCaptcha } from 'vue-recaptcha-v3';

const { RECAPTCHA_SITE_KEY: siteKey } = process.env

Vue.use(VueReCaptcha, { siteKey });
