import Vue from 'vue';
import Vuex, { MutationTree, GetterTree } from 'vuex';
import { ipAddressState } from './type';

Vue.use(Vuex);

const store = new Vuex.Store({
    state: (): ipAddressState => ({
        ipAddress: '127.0.0.1',
    }),
    mutations: {
        UPDATE_IPADDRESS(state, newip) {
            state.ipAddress = newip;
        },
    } as MutationTree<ipAddressState>,
    getters: {
        myIpAddress: (state): string => state.ipAddress,
    } as GetterTree<ipAddressState, ipAddressState>,
});

export function userStore() {
    return store;
}
